"use strict";
let resultado = 0;
let dioUnError = false;

/* Números y operacion a realizar. */
let numero = 4;
let numero2 = 2;
let operacion = "+";

if (operacion === "+") {
  resultado = numero + numero2;
} else if (operacion === "-") {
  resultado = numero - numero2;
} else if (operacion === "*") {
  resultado = numero * numero2;
} else if (operacion === "/") {
  /*Compruebo si se divide entre 0*/
  if (numero2 != 0) {
    resultado = numero / numero2;
  } else {
    /* error al dividir entre 0*/
    dioUnError = true;
  }
} else if (operacion === "**") {
  resultado = numero ** 2;
} else {
  /* mensaje sino es correcta la operacion*/
  console.log("No se ha seleccionado un tipo de operación correcta.");
  dioUnError = true;
}
/* Muestro resultado sino hay error */

if (dioUnError) {
  console.log("No se puede realizar la operación.");
} else {
  if (operacion === "**") {
    console.log("El resultado de " + numero + " elevado a 2 es " + resultado);
  } else {
    console.log(
      "El resultado de " + numero + operacion + numero2 + " es " + resultado
    );
  }
}
