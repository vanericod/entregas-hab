"use strict";
let equipo1 = [62, 34, 55];
let equipo2 = [35, 60, 59];
let equipo3 = [40, 39, 63];

function calcularMedia(arrNumero) {
  let suma = 0;
  for (let i = 0; i <= arrNumero.length - 1; i++) {
    suma += arrNumero[i];
  }
  return suma / arrNumero.length;
}

let mediaEquipo1 = calcularMedia(equipo1);
let mediaEquipo2 = calcularMedia(equipo2);
let mediaEquipo3 = calcularMedia(equipo3);

console.log(mediaEquipo1);
console.log(mediaEquipo2);
console.log(mediaEquipo3);

if (mediaEquipo1 > mediaEquipo2) {
  if (mediaEquipo1 > mediaEquipo3) {
    console.log("Equipo1 gana con " + mediaEquipo1);
  } else {
    console.log("Equipo3 gana con " + mediaEquipo3);
  }
} else {
  /* estoy aqui pq Equipo 2 es mayor 1*/
  if (mediaEquipo2 > mediaEquipo3) {
    console.log("Equipo2 gana con " + mediaEquipo2);
  } else {
    console.log("Equipo3 gana  con " + mediaEquipo3);
  }
}
