"use strict";

function tiraDado() {
  let miValor = Math.random() * 5;
  miValor = Math.floor(miValor) + 1;
  return miValor;
}

let puntuacion = 0; // esto es z
let tiradas = 0; //esto es x
let ultimaTirada = 0; //esto es y
const puntuacionMaxima = 50;

while (puntuacion < puntuacionMaxima) {
  ultimaTirada = tiraDado();
  puntuacion += ultimaTirada;
  tiradas++;
  console.log(puntuacion);
  console.log(
    "Tirada " +
      tiradas +
      ": " +
      "ha salido un " +
      ultimaTirada +
      " . Tienes un total de " +
      puntuacion +
      " puntos."
  );
}
console.log(
  "¡Enhorabuena, ha salido un " +
    ultimaTirada +
    "! ¡Has ganado un total de " +
    puntuacion +
    " puntos!"
);
