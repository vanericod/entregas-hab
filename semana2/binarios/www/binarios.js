"use strict";
function BinaryConverter(str) {
  let posicionBinario = 0;
  let arrBinario = str.split("");
  let resultadoFinal = 0;

  for (let i = arrBinario.length - 1; i >= 0; i--) {
    resultadoFinal += 2 ** posicionBinario * arrBinario[i];
    posicionBinario++;
    /*     console.log(posicionBinario);
     */
  }
  return resultadoFinal;
}
console.log("El número decimal es " + BinaryConverter("101"));
