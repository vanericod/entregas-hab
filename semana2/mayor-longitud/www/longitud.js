"use strict";

function letterCount(str) {
  let palabras = str.split(" ");
  let longitudMaxEncontrada = 0;
  let palabraGanadora = "";
  for (let i = 0; i < palabras.length; i++) {
    /*  console.log(palabras[i]);
    console.log(palabras[i].length); */

    if (palabras[i].length > longitudMaxEncontrada) {
      longitudMaxEncontrada = palabras[i].length;
      palabraGanadora = palabras[i];
    }
  }
  /* console.log(longitudMaxEncontrada);
  console.log(palabraGanadora); */
  return palabraGanadora;
}

let text = "Hoy es un día estupendo y fantástico";
letterCount(text);
console.log(letterCount(text));
