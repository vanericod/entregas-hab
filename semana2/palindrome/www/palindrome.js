"user strict";
let cadena = "Arriba la birra";

function reverseStr(str) {
  let cadenaInversa = "";
  for (let i = str.length - 1; i >= 0; i--) {
    cadenaInversa += str[i];
  }
  return cadenaInversa;
}

function palindromeTwo(str) {
  /* console.log(str.toLowerCase()); */
  let strNoSpaces = str.split(" ").join("");
  console.log(strNoSpaces);
  if (reverseStr(strNoSpaces).toLowerCase() === strNoSpaces.toLowerCase()) {
    return true;
  } else {
    return false;
  }
}
console.log(palindromeTwo(cadena));
